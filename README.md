## OpenShift Documentation

The OpenShift cartridge documentation can be found at:

* [`nodejs`](http://openshift.github.io/documentation/oo_cartridge_guide.html#nodejs)
* [`mongodb`](http://openshift.github.io/documentation/oo_cartridge_guide.html#mongodb)

## MongoDB

Make a `data` dir for local database and use `mongod-start` to run MonogDB.

## Deploying to OpenShift

Use `grunt buildcontrol:openshift`.